# Missed-prayers-calculator
###In the name of God, the Most Gracious, the Most Merciful

All praise for Allah Who has ordered us to establish prayer, the act of worship and direct communication with Allah almighty.
May the peace and blessings of Allah be upon Muhammad, the Messenger of Allah who delivered Orders of Allah and
taught us to perform prayer in the best possible way.

Assalamu alaikum, dear Brother/Sister, the purpose of this application is to help Muslims who missed a lot of prayers, to track and complete their missed prayers.

There are a lot of applications which help to track missed prayers. Those applications are very good and easy to use and understand. But i could not find some functions in those applications. Therefore I decided to build more advanced tracking app where every missed prayer will be treated as a specific object and provide detailed information about missed prayer.

Please feel completely free to edit this code, extend this code. And please, remember me in your prayers, so i can complete this application as i wanted it to be.

Simple android application for tracking and extinguishing missed prayers with feature to export the number of missed prayers to external devices. 
