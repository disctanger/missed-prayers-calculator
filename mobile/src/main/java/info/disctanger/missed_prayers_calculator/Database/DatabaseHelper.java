package info.disctanger.missed_prayers_calculator.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.sql.Date;
import java.text.SimpleDateFormat;

import info.disctanger.missed_prayers_calculator.Prayer;

public class DatabaseHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "Prayers.db";
    public static final String PRAYERS_TABLE_NAME = "prayers";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NAME";
    public static final String COL_3 = "MISSED_DATE";
    public static final String COL_4 = "PRAYED_DATE";
    public static final String COL_5 = "IS_COMPLETED";
    public static final String COL_6 = "CREATED_DATE";
    public static final String COL_7 = "UPDATED_DATE";
    public static final String TABLE_INIT_SQL = "create table " + PRAYERS_TABLE_NAME + " ("+
                                                                COL_1 + "INTEGER PRIMARY KEY," +
                                                                COL_2 + " TEXT, " +
                                                                COL_3 + " DATETIME, " +
                                                                COL_4 + " DATETIME, " +
                                                                COL_5 + " BOOLEAN," +
                                                                COL_6 + " DATETIME, " +
                                                                COL_7 + " DATETIME)";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_INIT_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean updatePrayer (Prayer prayer) {
        ContentValues contentValues = new ContentValues();

        String missedDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(prayer.getMissedDate());
        String createdDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(prayer.getCreatedDate());
        String updatedDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(prayer.getUpdatedDate());

        contentValues.put(COL_1, prayer.getId());
        contentValues.put(COL_2, prayer.getPrayerNameID());
        contentValues.put(COL_3, missedDateString);
        contentValues.put(COL_6, createdDateString);
        contentValues.put(COL_7, updatedDateString);
        boolean result = insertToDb(contentValues);
        return result;
    }

    public boolean removePrayer (Prayer prayer) {
        int id = prayer.getId();

        boolean result = removeFromDb(id);

        return result;
    }

    public boolean addPrayer (Prayer prayer) {
        ContentValues contentValues = new ContentValues();

        String missedDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(prayer.getMissedDate());
        String createdDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(prayer.getCreatedDate());
        String updatedDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(prayer.getUpdatedDate());

        String id = String.valueOf(prayer.getPrayerNameID());
        contentValues.put(COL_2, prayer.getPrayerNameID());
        contentValues.put(COL_3, missedDateString);
        contentValues.put(COL_6, createdDateString);
        contentValues.put(COL_7, updatedDateString);
        boolean result = updateDb(contentValues, id);
        return result;
    }

    public boolean insertToDb (ContentValues cv) {
        SQLiteDatabase db = this.getWritableDatabase();

        long result = db.insert(PRAYERS_TABLE_NAME,null, cv);
        if(result == -1)
            return false;
        else
            return true;
    }

    public boolean updateDb (ContentValues cv, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        long result = db.update(PRAYERS_TABLE_NAME,cv, "ID=" + id, null);
        if(result == -1)
            return false;
        else
            return true;
    }

    public boolean removeFromDb (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(PRAYERS_TABLE_NAME,"ID="+ id,null);

        if(result == -1)
            return false;
        else
            return true;
    }
}
