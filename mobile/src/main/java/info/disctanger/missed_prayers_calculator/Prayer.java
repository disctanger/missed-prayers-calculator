package info.disctanger.missed_prayers_calculator;

import java.text.SimpleDateFormat;
import java.util.Date;

import static info.disctanger.missed_prayers_calculator.PrayerNames.PRAYER_NAMES;

public class Prayer {

	private int id;
	private int prayerNameID;
	private String prayerName;
	private boolean isCompleted;
	private Date completedDate;
	private Date missedDate;
	private Date createdDate;
	private Date updatedDate;

	Prayer(int prayerNameID, Date missedDate) {
		this.setMissedDate(missedDate);

		this.setPrayerNameID(prayerNameID);

		String db_id_as_string = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(missedDate) + prayerNameID;
		int db_id = Integer.parseInt(db_id_as_string);
		this.setId(db_id);

		this.setPrayerName(PRAYER_NAMES[prayerNameID-1]);
	}

	public int getPrayerNameID() {
		return prayerNameID;
	}

	public void setPrayerNameID(int prayerNameID) {
		this.prayerNameID = prayerNameID;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void markAsCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public Date getMissedDate() {
		return missedDate;
	}

	public void setMissedDate(Date missedDate) {
		this.missedDate = missedDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrayerName() {
		return prayerName;
	}

	public void setPrayerName(String prayerName) {
		this.prayerName = prayerName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
