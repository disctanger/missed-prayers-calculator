package info.disctanger.missed_prayers_calculator;

import java.lang.reflect.Array;

public class PrayerNames {

	static final String DAWN = "DAWN";
	static final String NOON = "NOON";
	static final String AFTERNOON = "AFTERNOON";
	static final String SUNSET = "SUNSET";
	static final String NIGHT = "NIGHT";

	static String[] PRAYER_NAMES = {
			DAWN,
			NOON,
			AFTERNOON,
			SUNSET,
			NIGHT
	};
}
